<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use KDA\LicenseManager\Controllers\SatisAuthenticationController;

Route::post('/api/satis/authenticate',SatisAuthenticationController::class);