<?php

namespace KDA\LicenseManager\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\LicenseManager\Models\LicenceKey;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\KDA\LicenseManager\Models\LicenceKey>
 */
class LicenceKeyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LicenceKey::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
