<?php

namespace KDA\LicenseManager\Database\Factories;

use KDA\LicenseManager\Models\License;
use Illuminate\Database\Eloquent\Factories\Factory;

class LicenseFactory extends Factory
{
    protected $model = License::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
