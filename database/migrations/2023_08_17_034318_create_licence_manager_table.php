<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('licenses', function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs('licensable');
            $table->nullableMorphs('licensee');
            $table->timestamp('expires_at')->nullable();
            $table->string('key');
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('licenses');
     
        Schema::enableForeignKeyConstraints();
    }
};