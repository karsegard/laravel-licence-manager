<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\LicenseManager\Facades\LicenseManager;
use KDA\LicenseManager\Models\LicenseKey;
use KDA\LicenseManager\Models\License;
use KDA\Tests\Models\Post;
use KDA\Tests\Models\User;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
  use RefreshDatabase;

  /** @test */
  function licence_created()
  {
    // $o = License::factory()->create([]);
    $user = User::factory()->create();
    $post = Post::factory()->create();
    $l = new License();
    $l->licensee()->associate($user);
    $l->licensable()->associate($post);
    $l->save();
    $this->assertNotNull($l);
  }

  /** @test */
  function licence_key_created()
  {
    // $o = License::factory()->create([]);
    $user = User::factory()->create();
    $post = Post::factory()->create();
    $l = new License();
    $l->licensee()->associate($user);
    $l->licensable()->associate($post);
    $l->save();

    $k = new LicenseKey();
    $k->license()->associate($l);
    $k->key = 'fabien';
    $k->save();
    $this->assertNotNull($k);
  }

  /** @test */
  function facade()
  {
    $user = User::factory()->create();
    $post = Post::factory()->create();
    $l = LicenseManager::createLicense($user, $post, 'helloworld');
    $this->assertNotNull($l);
  }

  /** @test */
  function facade_check()
  {
    $user = User::factory()->create();
    $post = Post::factory()->create();
    $l = LicenseManager::createLicense($user, $post, 'helloworld');
    $this->assertNotNull($l);
    //dump($l->key.':'.$l->keys->first()->key);
    $result = LicenseManager::check($l->key.':'.$l->keys->first()->key);
    $this->assertTrue($result);
    $result = LicenseManager::check($l->key.':gnmagnag');
    $this->assertFalse($result);
  }
}
