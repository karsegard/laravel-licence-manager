Copyright 2023 Karsegard Digital Agency Sàrl
Copyright 2023 Fabien Karsegard
Copyright 2023 Emilie Karsegard

cf: https://www.karsegard.ch/conditions.pdf

1: Le Client acquiert de Karsegard Digital Agency Sàrl le droit d’utiliser les éléments graphiques fournis
par le Prestataire exclusivement pour ses besoins, pour le territoire d’implantation
du Client au moment de la réalisation et pour toute la durée d'utilisation du Projet,
sans être autorisé à les reproduire ou à les adapter, sauf autorisation explicite écrite
du Prestataire.

2: Karsegard Digital Agency Sàrl accorde au Client une License d’utilisation incessible et
intransmissible du code des logiciels créés pour le Projet et pour le territoire d’implantation
du Client au moment de la réalisation.

3: Les éléments externes, utilisées par le Prestataire restent la propriété exclusive de
leurs auteurs
