<?php
namespace KDA\LicenseManager\Models\Contracts;

interface Licensee
{

    public function getLicenseUsername():string;

}