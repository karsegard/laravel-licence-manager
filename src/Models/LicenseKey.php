<?php

namespace KDA\LicenseManager\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LicenseKey extends Model
{
    use HasFactory;
    protected $fillable=[
        'name',
        'key',
        'license_id'
    ];
    public function license(){
        return $this->belongsTo(License::class);
    }
}
