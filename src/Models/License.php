<?php

namespace KDA\LicenseManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;

class License extends Model
{
    use HasDefaultAttributes;
    use HasFactory;

    protected $fillable = [
        'licensee_type',
        'licensee_id',
        'key',
    ];

    protected $appends = [];

    protected $casts = [];


    public function createDefaultAttributes()
    {
        $this->defaultAttribute('key', (string) \Str::uuid());
    }

    protected static function newFactory()
    {
        return  \KDA\LicenseManager\Database\Factories\LicenseFactory::new();
    }

    public function licensable()
    {
        return $this->morphTo();
    }

    public function licensee()
    {
        return $this->morphTo();
    }

    public function keys()
    {
        return $this->hasMany(LicenseKey::class);
    }

    /*
    public function getTable()
    {
     
    }
*/
}
