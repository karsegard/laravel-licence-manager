<?php
namespace KDA\LicenseManager\Models\Traits;

trait Licensee 
{

    public function getLicenseUsername():string{
        return $this->email;
    }
}