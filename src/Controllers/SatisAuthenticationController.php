<?php

namespace KDA\LicenseManager\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use KDA\LicenseManager\Facades\LicenseManager;

class SatisAuthenticationController extends Controller
{
    public function __invoke(Request $request)
    {
        $licenseKey = $request->getPassword();
        $user = $request->getUser();
        Log::info('attempt from ' . $user);
        $uri = $request->header('X-Original-URI');
        Log::info('attempt for ' . $uri);

        //dist/kda/laravel-timesheet/kda-laravel-timesheet-4c1d625123f98eb2feb11be1aeceac6e72f4ae35-zip-923dfe.zip

        
        $pkg = explode('/', $uri);
        $package=null;
        if(count($pkg)>=4){
           $package = $pkg[2] . '/' . $pkg[3];
        }
        Log::info('attempt for ' . $package);
        $license = LicenseManager::check($licenseKey);
        abort_unless($license, 401, 'License key invalid');
        return response()->json('valid', 200);
     
    }
}
