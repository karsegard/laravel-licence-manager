<?php
namespace KDA\LicenseManager;

use Illuminate\Database\Eloquent\Model;
use KDA\LicenseManager\Models\License;
use KDA\LicenseManager\Models\LicenseKey;

//use Illuminate\Support\Facades\Blade;
class LicenseManager 
{
    

    public function createLicense(Model $licensable, Model $licensee,string | array $key):License
    {
        $l = new License();
        $l->licensable()->associate($licensable);
        $l->licensee()->associate($licensee);
        
        if(is_string($key)){
            $key = [$key];
        }

        $l->save();
        foreach($key as $_key){
            $k = new LicenseKey();
            $k->key = $_key;
            $k->license()->associate($l);
            $k->save();
        }

        return $l;
    }


    public function check ($key){
        $result = explode(':',$key);
        \Log::warning('invalid key provided');
        if(count($result)<2){
            return false;
        }
        list($license_key,$key)= $result;
        if(blank($license_key) || blank($key)){
            throw new \Exception('invalid key string');
        }

        $license = License::where('key',$license_key)->first();
        
        if($license){
            return  $license->keys->where('key',$key)->count()>0;
        }
        
        return false;
    }
}