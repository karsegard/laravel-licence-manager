<?php
namespace KDA\LicenseManager;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasDumps;
//use Illuminate\Support\Facades\Blade;
use KDA\LicenseManager\Facades\LicenseManager as Facade;
use KDA\LicenseManager\LicenseManager as Library;
    use KDA\Laravel\Traits\HasLoadableMigration;
use KDA\Laravel\Traits\HasMigration;
use KDA\Laravel\Traits\HasRoutes;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasLoadableMigration;
    use HasMigration;
    use HasRoutes;
    use HasDumps;
    protected $packageName ='laravel-License-manager';
    protected $routes=['web.php'];

    protected $dumps = [
        'licenses',
        'license_keys'
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
